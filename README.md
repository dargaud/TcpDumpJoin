# TcpDumpJoin

Joins lines output by 'tcpdump -tx' so you can grep them

Compile with: gcc -o TcpDumpJoin TcpDumpJoin.c Bin2ascii.c (see https://gitlab.com/dargaud/bin2ascii)

Use with: tcpdump options -tx

For instance: sudo tcpdump -i usbmon3 -txc 20 | TcpDumpJoin

or when reverse-engineering an ethernet protocol via a USB-to-ethernet dongle:  
sudo tcpdump -i usbmon3 -tx 2>test | TcpDumpJoin -H | grep " 3:9:[23] [0-9a-f]"
