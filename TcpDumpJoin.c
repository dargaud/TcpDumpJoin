// Compile with: gcc -o TcpDumpJoin TcpDumpJoin.c Bin2ascii.c
// Use with: tcpdump options -tx
// Also use "modprobe usbmon" if you want to intercept usb frames
// For instance: sudo tcpdump -i usbmon3 -txc 20 | TcpDumpJoin
// or: sudo tcpdump -i usbmon3 -tx 2>test | TcpDumpJoin -H | grep " 3:9:[23] [0-9a-f]"

#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>
#include <iso646.h>
#include <ctype.h>

#include "Bin2ascii.h"

static unsigned char CB[BUFSIZ*256]={0}, CL[16]={0};
static int Nb=0;

char* StripEndingCtrl(char* Str) {
	int L=strlen(Str);
	while (L>0 and iscntrl(Str[L-1])) Str[--L]='\0';
	return Str;
}

int main(int argc, char **argv) {
	char buf[BUFSIZ];
	int i, NoHex=0, Hex=0, Name=0, Ctrl=0, Esc=0;
	
	for (i=1; i<argc; i++) {
		if (0==strcmp("-h", argv[i])) {
			fprintf(stderr, "%s [-h] [-H] [-N] [-C] [-E]\n"\
							"\tJoins lines output by 'tcpdump -tx' so you can grep them.\n"
							"\t-h\tThis help\n"
							"\t-0\tDo not display preceeding hex codes\n"
							"\t-H\tDisplay non-ascii chars as backslashed hex codes\n"
							"\t-N\tDisplay chars 0~31 and 127 as names\n"
							"\t-C\tDisplay chars 0~31 as ^ controls\n"
							"\t-E\tDisplay chars 7~13 as \\ escapes\n"
							"\tBy default non-ascii are displayed as '.', each of the options overrides the ones above.\n"
							, argv[0]);
			return 1; 
		}
		else if (0==strcmp("-0", argv[i])) NoHex=1;
		else if (0==strcmp("-H", argv[i])) Hex=1;
		else if (0==strcmp("-N", argv[i])) Name=1;
		else if (0==strcmp("-C", argv[i])) Ctrl=1;
		else if (0==strcmp("-E", argv[i])) Esc=1;
		else { fprintf(stderr, "Unknown option %s\n", argv[i]); return 2; }
	}
	
	while (fgets(buf, BUFSIZ, stdin)) {
		//puts(buf);
		//printf(">>>%s<<<", buf);

		// Line is either a 'from', a 'to' or a hex dump
		if (strstr(buf, " to ") or strstr(buf, " from ")) { 
			// Display ascii version of current block
			printf(" %s\n", Bin2ascii((char*)CB, Nb, Hex, Name, Ctrl, Esc));
			fflush(stdout);							// Optional, useful if there are few messages
			Nb=0; memset(CL, 0, sizeof(CL)); 
			StripEndingCtrl(buf); printf("%s", buf); 
		}
		else {
			int Addr=0, n=0, i;
			// 0x0030:  18c3 5144 5018 1e00 2134 0000 0206 3336
			n=sscanf(buf, "%x: %02hhx%02hhx %02hhx%02hhx %02hhx%02hhx %02hhx%02hhx"
							 " %02hhx%02hhx %02hhx%02hhx %02hhx%02hhx %02hhx%02hhx",
					 &Addr, &CL[0], &CL[1], &CL[2], &CL[3], &CL[4], &CL[5], &CL[6], &CL[7], 
						&CL[8], &CL[9], &CL[10], &CL[11], &CL[12], &CL[13], &CL[14], &CL[15]);
			if (n<=1) printf("\nUnknown line format: %s", buf);
			for (i=0; i<n-1; i++) { 
				CB[Nb++]=CL[i]; 
				if (!NoHex) printf(" %02x", CL[i]); 
			}
		}
	}
	putchar('\n');
	return 0;
}
